//! This example exists mostly to showcase that you really can recurse as deep
//! as you want (or at least until your heap memory complains).

mod dizzy;

use vuot::{run, Stack};

fn main() {
    let result = dizzy::block_on(run(do_count));
    println!("{result}");
}

/// [`vuot::StacklessFn`] is implemented for functions returning futures, and
/// `async fn`s happen to generate a generic enough implementation for this to
/// work.
async fn do_count(stack: Stack<'_>) -> u64 {
    count(stack, 100_000_000).await
}

async fn count(stack: Stack<'_>, n: u64) -> u64 {
    if n == 0 {
        n
    } else {
        stack.run(count(stack, n - 1)).await + 1
    }
}
