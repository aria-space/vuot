mod dizzy;

use vuot::{run, Stack, StacklessFn};

fn main() {
    dizzy::block_on(async {
        let n = 40;
        println!("fib(40) = {}", run(Fib(&n)).await);
    });
}

struct Fib<'local>(&'local usize);
impl<'a> StacklessFn<'a, usize> for Fib<'_> {
    async fn call(self, stack: Stack<'a>) -> usize {
        fib(stack, *self.0).await
    }
}

async fn fib(stack: Stack<'_>, n: usize) -> usize {
    if n < 2 {
        n
    } else {
        stack.run(fib(stack, n - 1)).await + stack.run(fib(stack, n - 2)).await
    }
}
