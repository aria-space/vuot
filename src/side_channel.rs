use std::cell::Cell;
use std::future::Future;
use std::ptr::NonNull;

/// This is used to store type-erased futures that write their results through a
/// pointer instead of returning them.
pub(crate) trait SideChannelFuture: Future<Output = ()> {
    /// Return a type-erased pointer to the result `Cell<Option<T>>` returned
    /// produced by this future.
    ///
    /// # Safety
    ///
    /// The returned pointer points to a `Cell<Option<T>>` for the specific `T`
    /// of this future, and is only valid while the reference is being held.
    unsafe fn get_result_pointer(&self) -> NonNull<Cell<Option<()>>>;
}
